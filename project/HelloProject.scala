import sbt._
import sbt.Keys._
import xml.Elem
import xml.transform.RuleTransformer
import scala.xml.transform.RewriteRule 
object HelloProject extends Build {

  type XE = scala.xml.Elem
  type XN = scala.xml.Node
  type XNS = scala.xml.NodeSeq

  val updatePom = new RewriteRule{
    override def transform(n:XN):XNS = n match {
      case e:XE if(e.label == "scalaVersion") => <scalaVersion>2.9.2</scalaVersion>
      case _ => n
    }}


  val ppp: (scala.xml.Node => scala.xml.Node) = (a: scala.xml.Node) => {
    new RuleTransformer(updatePom)(a)
  }

  lazy val root = Project(id = "hello",
    base = file("."),
    settings = mySettings)


  lazy val mySettings = Project.defaultSettings ++ Seq(

    pomExtra := (
      <url>https://bitbucket.org/sentimental/poc_sbt</url>
        <licenses>
          <license>
            <name>BSD-style</name>
            <url>http://www.opensource.org/licenses/bsd-license.php</url>
            <distribution>repo</distribution>
          </license>
        </licenses>
        <scm>
          <url>git@bitbucket.org:sentimental/poc_sbt.git</url>
          <connection>scm:git@bitbucket.org:sentimental/poc_sbt.git</connection>
        </scm>
        <developers>
          {Seq(
          ("sentimental", "Bryan Hunt")
        ).map {
          case (id, name) =>
            <developer>
              <id>
                {id}
              </id>
              <name>
                {name}
              </name>
              <url>https://bitbucket.org/
                {id}
              </url>
            </developer>
        }}
        </developers>
        <build>
          <plugins>
            <plugin>
              <groupId>net.alchim31.maven</groupId>
              <artifactId>scala-maven-plugin</artifactId>
            </plugin>
            <plugin>
              <groupId>org.apache.maven.plugins</groupId>
              <artifactId>maven-surefire-plugin</artifactId>
            </plugin>
          </plugins>

          <pluginManagement>
            <plugins>
              <plugin>
                <groupId>net.alchim31.maven</groupId>
                <artifactId>scala-maven-plugin</artifactId>
                <version>3.0.2</version>
                <executions>
                  <execution>
                    <id>compile</id>
                    <goals>
                      <goal>compile</goal>
                    </goals>
                    <phase>compile</phase>
                  </execution>
                  <execution>
                    <id>test-compile</id>
                    <goals>
                      <goal>testCompile</goal>
                    </goals>
                    <phase>test-compile</phase>
                  </execution>
                  <execution>
                    <id>project-resources-execution</id>
                    <phase>process-resources</phase>
                    <goals>
                      <goal>compile</goal>
                    </goals>
                  </execution>
                </executions>
                <configuration>
                  <scalaVersion/>
                </configuration>
                <dependencies>
                  <dependency>
                    <groupId>org.scala-lang</groupId>
                    <artifactId>jline</artifactId>
                    <version>2.9.0-1</version>
                  </dependency>
                </dependencies>
              </plugin>
              <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <version>2.8</version>
                <configuration>
                  <useFile>false</useFile>
                  <disableXmlReport>true</disableXmlReport>
                  <excludes>
                    <exclude>**/*IntegrationTest*</exclude>
                  </excludes>
                  <includes>
                    <include>**/*Test.*</include>
                    <include>**/*Suite.*</include>
                  </includes>
                </configuration>
                <dependencies>
                  <!-- Force using the latest JUnit 47 provider -->
                  <dependency>
                    <groupId>org.apache.maven.surefire</groupId>
                    <artifactId>surefire-junit47</artifactId>
                    <version>2.8</version>
                  </dependency>
                </dependencies>
              </plugin>
              <plugin>
                <groupId>org.zeroturnaround</groupId>
                <artifactId>jrebel-maven-plugin</artifactId>
                <version>1.1.1</version>
                <executions>
                  <execution>
                    <id>generate-rebel-xml</id>
                    <phase>process-resources</phase>
                    <goals>
                      <goal>generate</goal>
                    </goals>
                  </execution>
                </executions>
              </plugin>
            </plugins>
          </pluginManagement>
        </build> ),

      pomPostProcess := (ppp
      )
  )
}
